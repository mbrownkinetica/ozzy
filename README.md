# Ozzy

**Version** 0.5.0

Ozzy is a GPX XML to CSV converter, originally developed to convert the OSM GPX Traces dataset.

Ozzy will recursively walk through the input directory it is given and exactly reproduce it in the output directory you specify.

## Example

`./bin/ozzy -i="./test-data/nested-data/" -o="./outputs/" -c -t=8`

## Flags

`-i="./input-directory/"`

Tells Ozzy which input directory you want to parse for GPX files.

**Note**: This can be either a directory of GPX files, or a nested heirarchy of directories containing GPX files.

`-o="./output-directory/"`

Tells Ozzy which base directory you want your CSV files output.

**Note**: The directory structure inside of this specified path will exactly mimic the input directory structure.

`-c`

Tells Ozzy to run in concurrent mode when included. If no `-c` is provided, serial compute mode is assumed.

`-t=3`

Tells Ozzy how many concurrent workers you want to use. Defaults to 3.

`-h=true`

Tells Ozzy if you want to include Kinetica column headers row. Defaults to true.

`-concat=false`

Tells Ozzy to concatenate all GPX files in each nested directory into a single CSV output file. Defaults to false. DO NOT RUN THIS WITH CONCURRENT MODE TURNED ON. There aren't thread blockers in place to ensure the common output file is writable at this time. Use this mode without `-c` only.

## Test Data

You will find two folders filled with test GPX data in the `/test-data/` directory. One is a folder with a list of files, the other contains a nested folder structure.