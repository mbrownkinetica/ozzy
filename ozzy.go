package main

import (
	"encoding/xml"
	"flag"
	"fmt"
	"log"
	"os"
	"path/filepath"
	"strings"
	"sync"
)

// Job holds concurrent job information
type Job struct {
	ID      string
	inPath  string
	outPath string
}

// TrackPoint holds info about individual GPS track points
type TrackPoint struct {
	Latitude  string `xml:"lat,attr"`
	Longitude string `xml:"lon,attr"`
	Elevation string `xml:"ele"`
	Timestamp string `xml:"time"`
	Course    string `xml:"course"`
	Speed     string `xml:"speed"`
}

// XMLDepthGauge holds info about streamed xml
type XMLDepthGauge struct {
	Depth int
	Tags  []string
}

var inputDirFlag, outputDirFlag string
var concurrentModeFlag, includeHeadersFlag, concatModeFlag bool
var workersFlag int

var headers = []string{
	"GPXID|long|data|nullable",
	"Creator|string|data|char256|nullable",
	"TraceName|string|data|char256|nullable",
	"Latitude|float|data|nullable",
	"Longitude|float|data|nullable",
	"Timestamp|string|data|char256|nullable",
	"Elevation|float|data|nullable",
	"Course|float|data|nullable",
	"Speed|float|data|nullable",
}

func init() {
	flag.StringVar(&inputDirFlag, "i", "", "The input path for the OSM file. (Required)")
	flag.StringVar(&outputDirFlag, "o", "", "The output path for the OSM files. (Required)")
	flag.IntVar(&workersFlag, "t", 3, "The number of workers.")
	flag.BoolVar(&includeHeadersFlag, "h", true, "Whether or not to include Kinetica headers row.")
	flag.BoolVar(&concurrentModeFlag, "c", false, "Concurrent mode. Requires subdirectories.")
	flag.BoolVar(&concatModeFlag, "concat", false, "Concat mode. Concatenates all GPX files in a directory into a single CSV output for that directory.")

	flag.Parse()

	if inputDirFlag == "" || outputDirFlag == "" {
		log.Fatal("Need both input and output paths.")
		os.Exit(1)
	}
}

func main() {

	if !concurrentModeFlag {
		run()
	} else if concurrentModeFlag {
		runConcurrent()
	}
}

func run() {
	fmt.Println("Running non-concurrent parsing...")

	// Get file list
	filepath.Walk(inputDirFlag, func(path string, f os.FileInfo, err error) error {
		isGPX := hasExt(path, ".gpx")
		if !isGPX {
			return nil
		}

		gpxID := getGPXID(path)

		outPath := getOutputPath(inputDirFlag, outputDirFlag, gpxID, path)

		// Read the XML file into the two outputs
		convertXML(gpxID, path, outPath)

		return nil
	})
}

func runConcurrent() {
	fmt.Println("Running concurrent parsing...")
	wg := &sync.WaitGroup{}
	jobs := make(chan Job)

	// start workers
	allocateWorkers(workersFlag, wg, jobs, convertXML)

	// Get file list
	filepath.Walk(inputDirFlag, func(path string, f os.FileInfo, err error) error {
		isGPX := hasExt(path, ".gpx")
		if !isGPX {
			return nil
		}

		gpxID := getGPXID(path)

		outPath := getOutputPath(inputDirFlag, outputDirFlag, gpxID, path)

		// Read the XML file into the two outputs
		jobs <- Job{gpxID, path, outPath}
		return nil
	})
	close(jobs)

	// wait for workers to complete
	wg.Wait()
}

func getOutputPath(inputPath string, outputPath string, gpxID string, currentPath string) string {
	// Build the output path based on the input path
	inPath, _ := filepath.Abs(filepath.Dir(currentPath))
	baseDir := filepath.Base(inputPath)
	nestedPath := strings.SplitAfter(inPath, baseDir)

	outDirPath, _ := filepath.Abs(outputPath)
	outPath := outDirPath

	// If nested path
	if nestedPath[1] != "" {
		// Append path
		// outPath += nestedPath[1] + "/"
		outPath += "/"

		// If concat mode
		if concatModeFlag {
			// Use the dir name as the file name
			outPath += filepath.Base(nestedPath[1])
		}
	} else {
		// If not nested path
		// If in concat mode
		if concatModeFlag {
			// Use output dir name as file name
			outPath += filepath.Base(outDirPath)
		} else {
			// Otherwise, use the GPX ID as the file name
			outPath += gpxID
		}
	}
	outPath += ".csv"
	return outPath
}

func convertXML(gpxID string, inPath string, outPath string) {

	// If the file doesn't exist, create it, or append to the file
	f, err := os.OpenFile(outPath, os.O_APPEND|os.O_CREATE|os.O_WRONLY, 0644)
	if err != nil {
		baseName := filepath.Base(outPath)
		basePath := outPath[:len(outPath)-len(baseName)]
		os.MkdirAll(basePath, 0740)
		f, err = os.Create(outPath)
		checkError("ERRORRRR", err)
	}
	defer f.Close()

	// return

	inputFile, err := os.Open(inPath)

	// var outputFile *os.File
	// if concatModeFlag {
	// 	// Check if output file already exists
	// 	_, err := os.OpenFile(outPath, os.O_APPEND|os.O_WRONLY, 0644)
	// 	if err != nil {
	// 		outputFile, err = os.Create(outPath)
	// 		checkError("We got an error", err)
	// 	}
	// } else {
	// 	fmt.Println("Creating new file", outPath)
	// 	// Create a new csv for every GPX 1-to-1
	// 	outputFile, err = os.Create(outPath)
	// 	checkError("Cannot create output file", err)
	// }

	dg := XMLDepthGauge{}

	// Print headers
	if includeHeadersFlag {
		wtln := strings.Join(headers, ",") + "\n"
		f.WriteString(wtln)
	}

	// Create a decoder
	decoder := xml.NewDecoder(inputFile)

	var creator, trackName string

	// Loop over XML nodes
	for {
		t, _ := decoder.Token()
		if t == nil {
			break
		}
		switch se := t.(type) {
		case xml.StartElement:
			if se.Name.Local != "trkpt" {
				dg.Tags = append(dg.Tags, se.Name.Local)
				dg.Depth++
			}

			parentTag := getParentTag(&dg)
			if se.Name.Local == "name" && parentTag == "trk" {
			}

			if se.Name.Local == "gpx" {
				creator = getAttrValue(se.Attr, "creator")
			}

			if se.Name.Local == "name" && parentTag == "trk" {
				trackName = getTagValue(decoder, &se)
			}

			if se.Name.Local == "trkpt" {
				var p TrackPoint
				decoder.DecodeElement(&p, &se)
				writePointToCsv(f, gpxID, creator, trackName, p)
			}
		case xml.EndElement:
			dg.Tags = dg.Tags[:len(dg.Tags)-1]
		}
	}
}

////////////////////////////////
//	Helpers
////////////////////////////////

// #region Helpers

func getParentTag(dg *XMLDepthGauge) string {
	if len(dg.Tags) >= 2 {
		return dg.Tags[len(dg.Tags)-2]
	}
	return ""
}

func getTagValue(decoder *xml.Decoder, se *xml.StartElement) string {
	var s string
	decoder.DecodeElement(&s, se)
	return s
}

func getAttrValue(attrs []xml.Attr, name string) string {
	for _, a := range attrs {
		if a.Name.Local == name {
			return a.Value
		}
	}
	return xml.Attr{}.Value
}

func writePointToCsv(outFile *os.File, gpxID string, creator string, trackName string, point TrackPoint) {
	outArr := []string{
		gpxID,
		creator,
		trackName,
		point.Latitude,
		point.Longitude,
		point.Timestamp,
		point.Elevation,
		point.Course,
		point.Speed,
	}
	outString := strings.Join(outArr, ",")
	outString += "\n"
	outFile.WriteString(outString)
}

func allocateWorkers(maxWorkers int, wg *sync.WaitGroup, jobs chan Job, worker func(gpxID string, inPath string, outPath string)) {
	wg.Add(maxWorkers)
	for i := 1; i <= maxWorkers; i++ {
		go func(i int) {
			defer wg.Done()

			for j := range jobs {
				worker(j.ID, j.inPath, j.outPath)
			}
		}(i)
	}
}

func checkError(message string, err error) {
	if err != nil {
		log.Fatal(message, err)
	}
}

func getGPXAbsPath(inputDir string, file os.FileInfo) string {
	gpxPath, err := filepath.Abs(inputDir + file.Name())
	checkError("Could not get absolute path of GPX file: "+inputDir+file.Name(), err)
	return gpxPath
}

func getGPXID(gpxPath string) string {
	extSize := len(filepath.Ext(gpxPath))
	base := filepath.Base(gpxPath)
	gpxID := base[0 : len(base)-extSize]
	return gpxID
}

func hasExt(fPath string, ext string) bool {
	if filepath.Ext(fPath) != ext && filepath.Ext(fPath) != strings.ToUpper(ext) {
		return false
	}
	return true
}

func makeOutputDir(outputDirPath string) string {
	outputDir, err := filepath.Abs(outputDirPath)
	checkError("Could not get the output path", err)
	_, err = os.Stat(outputDir)
	if err != nil {
		_ = os.Mkdir(outputDir, os.ModePerm)
	}
	return outputDir
}

// #endregion Helpers
